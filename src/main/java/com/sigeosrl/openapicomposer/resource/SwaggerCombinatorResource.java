package com.sigeosrl.openapicomposer.resource;

import com.sigeosrl.openapicomposer.service.OpenApiAggregator;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/openapi")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SwaggerCombinatorResource {

    @Inject
    OpenApiAggregator aggregator;

    @GET
    @Path("/combined")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCombinedOpenApi() {
        return Response.ok(aggregator.getCombinedOpenAPI()).build();
    }
}
