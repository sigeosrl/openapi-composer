package com.sigeosrl.openapicomposer.service;

import io.quarkus.runtime.Startup;
import io.quarkus.runtime.annotations.RegisterForReflection;
import io.quarkus.scheduler.Scheduled;
import io.smallrye.openapi.api.models.*;
import io.smallrye.openapi.api.models.callbacks.CallbackImpl;
import io.smallrye.openapi.api.models.headers.HeaderImpl;
import io.smallrye.openapi.api.models.links.LinkImpl;
import io.smallrye.openapi.api.models.media.*;
import io.smallrye.openapi.api.models.parameters.ParameterImpl;
import io.smallrye.openapi.api.models.parameters.RequestBodyImpl;
import io.smallrye.openapi.api.models.responses.APIResponseImpl;
import io.smallrye.openapi.api.models.responses.APIResponsesImpl;
import io.smallrye.openapi.api.models.security.SecurityRequirementImpl;
import io.smallrye.openapi.api.models.servers.ServerImpl;
import io.smallrye.openapi.api.models.servers.ServerVariableImpl;
import io.smallrye.openapi.api.models.tags.TagImpl;
import io.smallrye.openapi.runtime.io.OpenApiParser;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Singleton;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.openapi.models.OpenAPI;
import org.eclipse.microprofile.openapi.models.Operation;
import org.eclipse.microprofile.openapi.models.PathItem;
import org.eclipse.microprofile.openapi.models.tags.Tag;
import org.jboss.logging.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Singleton
@Startup
@RegisterForReflection(targets = {
        OpenAPIImpl.class,
        PathsImpl.class,
        TagImpl.class,
        PathItemImpl.class,
        OperationImpl.class,
        APIResponsesImpl.class,
        APIResponseImpl.class,
        ContentImpl.class,
        MediaTypeImpl.class,
        SchemaImpl.class,
        RequestBodyImpl.class,
        ParameterImpl.class,
        EncodingImpl.class,
        HeaderImpl.class,                 // Adding additional necessary classes
        LinkImpl.class,                   // Adding LinkImpl
        CallbackImpl.class,               // Adding CallbackImpl
        DiscriminatorImpl.class,          // Adding DiscriminatorImpl
        ExternalDocumentationImpl.class,  // Adding ExternalDocumentationImpl
        XMLImpl.class,                    // Adding XMLImpl
        ComponentsImpl.class,             // Adding ComponentsImpl
        SecurityRequirementImpl.class,    // Adding SecurityRequirementImpl
        ServerImpl.class,                 // Adding ServerImpl
        ServerVariableImpl.class          // Adding ServerVariableImpl
})
public class OpenApiAggregator {

    private static final Logger LOGGER = Logger.getLogger(OpenApiAggregator.class);

    @ConfigProperty(name = "openapi.services")
    List<String> openAPIServices;

    @ConfigProperty(name = "openapi.urls")
    List<URL> openAPIUrls;

    OpenAPI combinedOpenAPI;

    @PostConstruct
    public void init() {
        fetchAndCombineOpenAPIs();
    }

    @Scheduled(every = "1h")
    public void fetchAndCombineOpenAPIs() {
        List<OpenAPI> openAPIs = openAPIUrls.stream()
                .map(url -> {
                    try {
                        return OpenApiParser.parse(url);
                    } catch (IOException e) {
                        LOGGER.error("Failed to parse OpenAPI from URL: " + url, e);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .toList();

        combinedOpenAPI = new OpenAPIImpl(); // Initialize or reset the combined OpenAPI object.
        combinedOpenAPI.setOpenapi("3.0.3"); // Set the OpenAPI version.
        combinedOpenAPI.setPaths(new PathsImpl()); // Initialize the paths object.

        // Initialize tags for each service
        for (String service : openAPIServices) {
            Tag serviceTag = new TagImpl();
            serviceTag.setName(service);
            combinedOpenAPI.addTag(serviceTag);
        }

        // Only add paths for allowed services
        for (int serviceCounter = 0; serviceCounter < openAPIs.size(); serviceCounter++) {
            String servicePrefix = openAPIServices.get(serviceCounter);
            OpenAPI openAPI = openAPIs.get(serviceCounter);
            if (servicePrefix == null || openAPI == null) {
                LOGGER.error("Service prefix or OpenAPI is null for service at index: " + serviceCounter);
                continue;
            }

            // Add paths from the OpenAPI to the combined OpenAPI
            for (String path : openAPI.getPaths().getPathItems().keySet()) {
                PathItem pathItem = openAPI.getPaths().getPathItem(path);
                if (pathItem == null) {
                    LOGGER.error("PathItem is null for path: " + path);
                    continue;
                }

                // Prefix the path with the service name
                String prefixedPath = "/" + servicePrefix + path;

                // Add the path item to the combined OpenAPI
                combinedOpenAPI.getPaths().addPathItem(prefixedPath, pathItem);

                // Add the tag to each operation in the path item
                addTagToOperations(pathItem, servicePrefix);
            }
        }
    }

    private void addTagToOperations(PathItem pathItem, String tag) {
        if (pathItem.getGET() != null) {
            addTagToOperation(pathItem.getGET(), tag);
        }
        if (pathItem.getPUT() != null) {
            addTagToOperation(pathItem.getPUT(), tag);
        }
        if (pathItem.getPOST() != null) {
            addTagToOperation(pathItem.getPOST(), tag);
        }
        if (pathItem.getDELETE() != null) {
            addTagToOperation(pathItem.getDELETE(), tag);
        }
        if (pathItem.getOPTIONS() != null) {
            addTagToOperation(pathItem.getOPTIONS(), tag);
        }
        if (pathItem.getHEAD() != null) {
            addTagToOperation(pathItem.getHEAD(), tag);
        }
        if (pathItem.getPATCH() != null) {
            addTagToOperation(pathItem.getPATCH(), tag);
        }
        if (pathItem.getTRACE() != null) {
            addTagToOperation(pathItem.getTRACE(), tag);
        }
    }

    private void addTagToOperation(Operation operation, String tag) {
        List<String> tags = new ArrayList<>();
        tags.add(tag); // Only add the service tag to the operation
        operation.setTags(tags);
    }

    public OpenAPI getCombinedOpenAPI() {
        return combinedOpenAPI;
    }
}
