package com.sigeosrl.openapicomposer.service;

import io.quarkus.runtime.Startup;
import io.quarkus.runtime.annotations.RegisterForReflection;
import io.smallrye.openapi.api.models.*;
import io.smallrye.openapi.api.models.callbacks.CallbackImpl;
import io.smallrye.openapi.api.models.headers.HeaderImpl;
import io.smallrye.openapi.api.models.links.LinkImpl;
import io.smallrye.openapi.api.models.media.*;
import io.smallrye.openapi.api.models.parameters.ParameterImpl;
import io.smallrye.openapi.api.models.parameters.RequestBodyImpl;
import io.smallrye.openapi.api.models.responses.APIResponseImpl;
import io.smallrye.openapi.api.models.responses.APIResponsesImpl;
import io.smallrye.openapi.api.models.security.SecurityRequirementImpl;
import io.smallrye.openapi.api.models.servers.ServerImpl;
import io.smallrye.openapi.api.models.servers.ServerVariableImpl;
import io.smallrye.openapi.api.models.tags.TagImpl;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.eclipse.microprofile.openapi.OASModelReader;
import org.eclipse.microprofile.openapi.models.OpenAPI;
import org.jboss.logging.Logger;

@Singleton
@Startup
@RegisterForReflection(targets = {
        OpenAPIImpl.class,
        PathsImpl.class,
        TagImpl.class,
        PathItemImpl.class,
        OperationImpl.class,
        APIResponsesImpl.class,
        APIResponseImpl.class,
        ContentImpl.class,
        MediaTypeImpl.class,
        SchemaImpl.class,
        RequestBodyImpl.class,
        ParameterImpl.class,
        EncodingImpl.class,
        HeaderImpl.class,                 // Adding additional necessary classes
        LinkImpl.class,                   // Adding LinkImpl
        CallbackImpl.class,               // Adding CallbackImpl
        DiscriminatorImpl.class,          // Adding DiscriminatorImpl
        ExternalDocumentationImpl.class,  // Adding ExternalDocumentationImpl
        XMLImpl.class,                    // Adding XMLImpl
        ComponentsImpl.class,             // Adding ComponentsImpl
        SecurityRequirementImpl.class,    // Adding SecurityRequirementImpl
        ServerImpl.class,                 // Adding ServerImpl
        ServerVariableImpl.class          // Adding ServerVariableImpl
})
public class CustomOpenAPIModelReader implements OASModelReader {

    @Inject
    OpenApiAggregator openApiAggregator;

    OpenAPI combinedOpenAPI;

    Logger logger = Logger.getLogger(CustomOpenAPIModelReader.class);


    @PostConstruct
    public void init() {
        openApiAggregator.init();
        combinedOpenAPI = openApiAggregator.getCombinedOpenAPI();
        logger.info("Initialized combined OpenAPI!!!");
    }

    @Override
    public OpenAPI buildModel() {
        if (combinedOpenAPI == null) {
            logger.error("Combined OpenAPI is null!!!");
            return new OpenAPIImpl();
        }
        return combinedOpenAPI;
    }
}