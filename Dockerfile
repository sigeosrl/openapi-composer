# Use a base image with a suitable OS (Ubuntu in this case)
FROM ubuntu:22.04 AS build

# Set environment variables for non-interactive installations
ENV DEBIAN_FRONTEND=noninteractive

# Install dependencies
RUN apt-get update && \
    apt-get install -y build-essential zlib1g-dev curl zip unzip && \
    apt-get clean

# Install SDKMAN
RUN curl -s "https://get.sdkman.io" | bash

# Make SDKMAN available in the current shell
RUN bash -c "source $HOME/.sdkman/bin/sdkman-init.sh && \
    sdk install java 21.0.4-graal"

# Set environment variables for GraalVM
ENV JAVA_HOME="/root/.sdkman/candidates/java/current"
ENV PATH="$JAVA_HOME/bin:$PATH"

# Copy your project files
COPY . /workspace
WORKDIR /workspace
RUN chmod +x gradlew

# Build the project and compile the native image
RUN ./gradlew build -x test -Dquarkus.package.type=native

# Second stage: runtime image
FROM ghcr.io/graalvm/jdk-community:21.0.2 AS runtime

# Copy the native executable to the runtime image
COPY --from=build /workspace/build/quarkus-build/app/openapi-composer-runner /app/openapi-composer-runner

# Set the entrypoint to execute your application
ENTRYPOINT ["/app/openapi-composer-runner", "-Dquarkus.http.host=0.0.0.0"]
